import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('kobiton-store:26820', true)

Mobile.tap(findTestObject('DemoObjects/android.widget.Button0 - Skip sign in'), 0)

Mobile.tap(findTestObject('DemoObjects/android.view.View0 - Clear search keywords'), 0)

Mobile.tap(findTestObject('DemoObjects/android.widget.Button0 - Open Menu'), 0)

Mobile.pressBack()

Mobile.closeApplication()

CustomKeywords.'DemoKW.hello'()

WebUI.callTestCase(findTestCase('Day4/SampleMobileTest1'), [:], FailureHandling.STOP_ON_FAILURE)

